import Data.Time

import GHC.IO.Exception

import System.Cmd
import System.Directory
import System.Environment
import System.IO

prependFile :: FilePath -> String -> IO ()
prependFile fpth str = do
    (tmpName, tmpHandle) <- openTempFile "." "temp"
    hPutStrLn tmpHandle str
    readFile fpth >>= hPutStr tmpHandle
    hClose tmpHandle
    removeFile fpth
    renameFile tmpName fpth

-- DT - date and time

monoStr :: Char -> Int -> String
monoStr chr len =
	take len $ repeat chr

lnsBeforeDTCnt :: Int
lnsBeforeDTCnt = 2

lnsBeforeDT :: String
lnsBeforeDT =
	take lnsBeforeDTCnt $ repeat '\n'

dTIndent :: String
dTIndent = take 4 $ repeat ' '

-- MSG - message

lnsBeforeMSG :: String
lnsBeforeMSG = "\n\n"

msgIndent :: String
msgIndent = take 8 $ repeat ' '

journalFile :: FilePath
journalFile = "Journal.txt"

formatDate :: String -> String
formatDate dt = lnsBeforeDT ++ (dTIndent ++ dt)

formatMsg  :: String -> String
formatMsg msg =
	(++) lnsBeforeMSG $
		unlines $ map ((++) msgIndent) $ lines msg

formatRecord :: String -> String -> String
formatRecord dt msg = (formatDate dt) ++ (formatMsg msg)

-- Returns date with local time
getDT = getZonedTime
	>>= return . zonedTimeToLocalTime

makeRecord :: String -> IO ()
makeRecord msg = do
	dt <- getDT >>= return . show
	prependFile journalFile
		$ formatRecord dt msg

-- RD - record

dropTopRD :: String -> String
dropTopRD jnl =
	let 
		jnlTail = drop (lnsBeforeDTCnt + 1) $ lines jnl
		jnlPop  = unlines $ dropWhile notDT $ jnlTail
		jnlNew  = lnsBeforeDT ++ jnlPop 
	in
		jnlNew
	where
		notDT :: String -> Bool
		notDT ln = null $
			(reads ln :: [(LocalTime, String)]) 

popRecord :: IO ()
popRecord = do
	(tmpName, tmpHandle) <- openTempFile "." "temp"
	readFile journalFile >>=
		hPutStr tmpHandle . dropTopRD
	hClose tmpHandle
	removeFile journalFile
	renameFile tmpName journalFile

showJournal :: IO ()
showJournal =
	readFile journalFile >>= putStrLn

parseErrMsg :: String
parseErrMsg =
	"\n    I realy can't understand what do you want from me...\n"

newMsgFileName :: String
newMsgFileName = "New message.txt"

emptyFile :: FilePath -> IO ()
emptyFile fpth = writeFile fpth ""

createEmptyMsgFile :: IO ()
createEmptyMsgFile =
	emptyFile newMsgFileName

deleteMsgFile :: IO ()
deleteMsgFile =
	removeFile newMsgFileName

editorPath :: String
editorPath = "notepad"

getMsgFromEditor :: IO String
getMsgFromEditor = do 
	rawSystem editorPath [newMsgFileName]
	readFile newMsgFileName

makeRecordWithEditor :: IO ()
makeRecordWithEditor = do
	createEmptyMsgFile
	getMsgFromEditor >>= makeRecord
	deleteMsgFile 

putParseErrMsg :: IO ()
putParseErrMsg = putStrLn parseErrMsg

parseInput :: [String] -> IO ()
parseInput ("add":rest) =
	case rest of
		[]             -> makeRecordWithEditor
		("-m":(msg:_)) -> makeRecord msg
		_              -> putParseErrMsg
parseInput ("pop":_)    = popRecord
parseInput ("show":_)   = showJournal
parseInput _	        = putParseErrMsg

isJnlExist :: IO Bool
isJnlExist =
	doesFileExist journalFile

main = do
	hasJnl <- isJnlExist
	if (not hasJnl)
		then emptyFile journalFile
		else return()
	getArgs >>= parseInput